// Promise Version
import { Injectable }              from '@angular/core';
import { Http, Response }          from '@angular/http';
import { Headers, RequestOptions, URLSearchParams } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Device } from './device';
import { User } from './user';
import { Tag } from './tag';

import { ApiService } from '../../app/api';

@Injectable()
export class DeviceService {
  // URL to web api
  private devicesUrl = 'http://api.pfm.alberto.cat/v1/devices';

  constructor (public api:ApiService, private http: Http) {}

  getDevices (): Promise<Device[]> {
    
    let headers = new Headers();
    headers.append('Authorization', "Bearer " + this.api.token);
    let options = new RequestOptions({ headers: headers });
    
    return this.http.get(this.devicesUrl, options)
                             .toPromise()
                             .then(this.extractItemsData)
                             .catch(this.handleError);
    /*
    return this.http.get(this.devicesUrl)
                    .toPromise()
                    .then(this.extractItemsData)
                    .catch(this.handleError);
    */
  }

  getDevice (id): Promise<Device> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    headers.append('Authorization', "Bearer " + this.api.token);
    let options = new RequestOptions({ headers: headers });
    
    return this.http.get(this.devicesUrl+'/'+id, options)
                    .toPromise()
                    .then(this.extractItemData)
                    .catch(this.handleError);
  }

  getTag(device_id, tag_id): Promise<Tag> {
    
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    headers.append('Authorization', "Bearer " + this.api.token);
    let options = new RequestOptions({ headers: headers });
    
    return this.http.get(this.devicesUrl+'/'+device_id+'/tags/'+tag_id, options)
                    .toPromise()
                    .then(this.extractItemData)
                    .catch(this.handleError);
  }
  
  getDeviceUsers (id): Promise<User[]> {
    
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    headers.append('Authorization', "Bearer " + this.api.token);
    let options = new RequestOptions({ headers: headers });
    
    return this.http.get(this.devicesUrl+'/'+id+'/users', options)
                    .toPromise()
                    .then(this.extractItemsData)
                    .catch(this.handleError);
  }

  addDevice (name: string, opc_path: string): Promise<Device> {
    
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    headers.append('Authorization', "Bearer " + this.api.token);
    let options = new RequestOptions({ headers: headers });
    
    let data = new URLSearchParams();
    data.append('name', name);
    data.append('opc_path', opc_path);
  
    return this.http.post(this.devicesUrl, data, options)
               .toPromise()
               .then(this.extractItemData)
               .catch(this.handleError);
  }

  addTag (device_id: number, name: string, group: string): Promise<Device> {
    
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    headers.append('Authorization', "Bearer " + this.api.token);
    let options = new RequestOptions({ headers: headers });
    
    let data = new URLSearchParams();
    data.append('name', name);
    data.append('group', group);
  
    return this.http.post(this.devicesUrl + '/' + device_id + '/tags', data, options)
               .toPromise()
               .then(this.extractItemData)
               .catch(this.handleError);
  }


  addDeviceUser (device_id: string, user: string, role_id: string): Promise<Device> {
    
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    headers.append('Authorization', "Bearer " + this.api.token);
    let options = new RequestOptions({ headers: headers });
    
    let data = new URLSearchParams();
    data.append('user', user);
    data.append('role_id', role_id);
  
    return this.http.post(this.devicesUrl + '/' + device_id + '/users/', data, options)
               .toPromise()
               .then(this.extractItemData)
               .catch(this.handleError);
  }

  
  
  editTag (device_id: number, tag_id: number, name: string, group: string): Promise<Device> {
    
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    headers.append('Authorization', "Bearer " + this.api.token);
    let options = new RequestOptions({ headers: headers });
    
    let data = new URLSearchParams();
    data.append('name', name);
    data.append('group', group);
  
    return this.http.put(this.devicesUrl + '/' + device_id + '/tags/' + tag_id, data, options)
               .toPromise()
               .then(this.extractItemData)
               .catch(this.handleError);
  }
    
  editDevice (id: number, name: string, opc_path: string): Promise<Device> {
    
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    headers.append('Authorization', "Bearer " + this.api.token);
    let options = new RequestOptions({ headers: headers });
    
    let data = new URLSearchParams();
    data.append('name', name);
    data.append('opc_path', opc_path);
  
    return this.http.put(this.devicesUrl + '/' + id, data, options)
               .toPromise()
               .then(this.extractItemData)
               .catch(this.handleError);
  }
    
  
  editDeviceUser (device_id: number, user_id: number, role_id: string): Promise<Device> {
    
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    headers.append('Authorization', "Bearer " + this.api.token);
    let options = new RequestOptions({ headers: headers });
    
    let data = new URLSearchParams();
    data.append('role_id', role_id);
  
    return this.http.put(this.devicesUrl + '/' + device_id + '/users/' + user_id, data, options)
               .toPromise()
               .then(this.extractItemData)
               .catch(this.handleError);
  }
  
  removeDevice (id: number): Promise<Device> {
    
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    headers.append('Authorization', "Bearer " + this.api.token);
    let options = new RequestOptions({ headers: headers });
    
    return this.http.delete(this.devicesUrl + '/' + id, options)
               .toPromise()
               .then(this.extractItemData)
               .catch(this.handleError);
  }
  
  removeTag (device_id: number, tag_id: number): Promise<Device> {
    
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    headers.append('Authorization', "Bearer " + this.api.token);
    let options = new RequestOptions({ headers: headers });
    
    return this.http.delete(this.devicesUrl + '/' + device_id + '/tags/' + tag_id, options)
               .toPromise()
               .then(this.extractItemData)
               .catch(this.handleError);
  }
  
  removeDeviceUser (device_id: number, user_id: number): Promise<Device> {
    
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    headers.append('Authorization', "Bearer " + this.api.token);
    let options = new RequestOptions({ headers: headers });
    
    return this.http.delete(this.devicesUrl + '/' + device_id + '/users/' + user_id, options)
               .toPromise()
               .then(this.extractItemData)
               .catch(this.handleError);
  }
  
  private extractItemsData(res: Response) {
    let body = res.json();
    return body.items || { };
  }
  
  private extractItemData(res: Response) {
    let body = res.json();
    console.log("here we go");
    return body || { };
  }

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      errMsg = body.devMessage ? body.devMessage : body.userMessage;
    } else {
      errMsg = error.devMessage ? error.devMessage : error.toString();
    }
    console.error(errMsg);
    alert("[device]: " + errMsg);
    return Promise.reject(errMsg);
  }

}
