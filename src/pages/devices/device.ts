export class Device {
  constructor(
    public id: number,
    public OPC_path: string,
    public groups: any[],
    public name: string) { }
}
