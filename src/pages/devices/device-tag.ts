import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Device }              from './device';
import { Tag }              from './tag';
import { DeviceService }       from './device.service';

import { ApiService } from '../../app/api';

@Component({
  selector: 'page-tag',
  templateUrl: 'device-tag.html',
  providers: [ DeviceService ],
})
export class DeviceTag {
  selectedItem: any;
  selectedTag: any;

  errorMessage: string;
  tag: Tag;
  mode = 'Observable';
  
  values: any[];
  
  constructor(public api:ApiService, private deviceService: DeviceService, public navCtrl: NavController, public navParams: NavParams) {
    this.selectedItem = navParams.get('device');
    this.selectedTag = navParams.get('tag');
  }
  
  ngOnInit() { this.getTag(this.selectedItem.id, this.selectedTag.id); }
  refreshValues() { this.getTag(this.selectedItem.id, this.selectedTag.id); }
  
  getTag(device_id, tag_id) {
    
                    this.api.getToken()
                        .then((value) => {
                            this.api.token = value.access_token;                    
                              
                            this.deviceService.getTag(device_id, tag_id)
                                             .then((value) => {
                                this.tag = value
                                this.values = this.tag.values;
                            }, (error) => {
                               this.errorMessage = <any>error
                            });
                    }, (error) => {
                      this.errorMessage = <any>error
                    });
  }
}
