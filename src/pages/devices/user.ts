export class User {
  constructor(
    public id: number,
    public nick: string,
    public role_id: number,
    public role: string) { }
}
