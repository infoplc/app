import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { DevicesList } from '../pages/devices/list';
import { DeviceView } from '../pages/devices/view';

import { DeviceUsers } from '../pages/devices/users';
import { DeviceTag } from '../pages/devices/device-tag';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


import { HttpModule, JsonpModule } from '@angular/http';

import { ApiService } from './api';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    DevicesList,
    DeviceUsers,
    DeviceTag,
    DeviceView
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    JsonpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    DevicesList,
    DeviceUsers,
    DeviceTag,
    DeviceView
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiService
  ]
})
export class AppModule {}
