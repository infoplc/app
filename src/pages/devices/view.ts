import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, AlertController, ActionSheetController, Platform } from 'ionic-angular';


import { Device }              from './device';
import { DeviceService }       from './device.service';

import { DeviceUsers } from './users';
import { DeviceTag } from './device-tag';

import { ApiService } from '../../app/api';
@Component({
  selector: 'page-view',
  templateUrl: 'view.html',
  providers: [ DeviceService ],
})
export class DeviceView {
  selectedItem: any;

  errorMessage: string;
  device: Device;
  mode = 'Observable';
  
  groups: Array<{name: string, tags: any[]}>;
  
  
  constructor(public api:ApiService, private deviceService: DeviceService, public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public platform: Platform, public actionSheetCtrl: ActionSheetController) {
    this.selectedItem = navParams.get('item');
  }
  
  escapeHtml(unsafe) {
    return unsafe.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;")
                 .replace(/"/g, "&quot;").replace(/'/g, "&#039;");
  }
  
  usersView() {
    this.navCtrl.push(DeviceUsers, {
      item: this.selectedItem
    });
  }
  
  test() {
    console.log(this.device);
  }

  itemTapped(event, item, group) {
    this.navCtrl.push(DeviceTag, {
      device: this.selectedItem,
      tag: item
    });
  } 
  
  
  ngOnInit() { this.getDevice(this.selectedItem.id); }
  
  refreshTags() {
    this.getDevice(this.selectedItem.id);
  }
  
  getDevice(id) {
    
                    this.api.getToken()
                        .then((value) => {
                            this.api.token = value.access_token;                    
        
                            this.deviceService.getDevice(id)
                                             .then((value) => {
                                this.device = value
                                this.groups = this.device.groups;
                            }, (error) => {
                               this.errorMessage = <any>error
                            });
                    }, (error) => {
                      this.errorMessage = <any>error
                    });
    
  }
  
  
  itemPressed(event, item, group) {
    let actionSheet = this.actionSheetCtrl.create({
      title: item.name,
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Eliminar',
          role: 'destructive',
          icon: !this.platform.is('ios') ? 'trash' : null,
          handler: () => {
            
            let alert = this.alertCtrl.create({
              title: 'Confirmar acción',
              message: '¿Seguro que quieres eliminar la etiqueta <b>'+this.escapeHtml(item.name)+'</b>?',
              buttons: [
                {
                  text: 'Cancelar',
                  role: 'cancel',
                  handler: () => {
                  }
                },
                {
                  text: 'Eliminar',
                  handler: () => {
                   
                    this.api.getToken()
                        .then((value) => {
                            this.api.token = value.access_token;                    
                                    
                             this.deviceService.removeTag(this.selectedItem.id, item.id)
                                            .then((value) => {
                                this.getDevice(this.selectedItem.id);         
                            }, (error) => {
                                this.errorMessage = <any>error
                            });
                            
                    }, (error) => {
                      this.errorMessage = <any>error
                    });
                        
                         
                  }
                }
              ]
            });
            alert.present();
                            
          }
        },
        {
          text: 'Editar',
          icon: !this.platform.is('ios') ? 'md-create' : null,
          handler: () => {
            
            let prompt = this.alertCtrl.create({
              title: 'Editar etiqueta',
              message: "Introduce los datos de la etiqueta",
              inputs: [
                {
                  name: 'name',
                  value: item.name,
                  placeholder: 'Nombre'
                },
                {
                  name: 'group',
                  value: group.name,
                  placeholder: 'Grupo'
                },
              ],
              buttons: [
                {
                  text: 'Cancelar',
                  handler: data => {
                    
                  }
                },
                {
                  text: 'Agregar',
                  handler: data => {
                         
                    this.api.getToken()
                        .then((value) => {
                            this.api.token = value.access_token;                    
                                    
                            this.deviceService.editTag(this.selectedItem.id, item.id, data.name, data.group)
                                            .then((value) => {
                                this.getDevice(this.selectedItem.id);  
                            }, (error) => {
                                this.errorMessage = <any>error
                            });
                            
                            
                    }, (error) => {
                      this.errorMessage = <any>error
                    });
                               
                  }
                }
              ]
            });
            prompt.present();
            
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
          }
        }
      ]
    });

    actionSheet.present();
}
  
  
  newTag() {
    let prompt = this.alertCtrl.create({
      title: 'Nueva etiqueta',
      message: "Introduce los datos de la etiqueta",
      inputs: [
        {
          name: 'name',
          placeholder: 'Nombre'
        },
        {
          name: 'group',
          placeholder: 'Grupo (opcional)'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            console.log('Cancel clicked');
            
          }
        },
        {
          text: 'Agregar',
          handler: data => {
                    
                    this.api.getToken()
                        .then((value) => {
                            this.api.token = value.access_token;                    
        
                            this.deviceService.addTag(this.device.id, data.name, data.group)
                                            .then((value) => {
                                this.getDevice(this.selectedItem.id);
                
                            }, (error) => {
                                this.errorMessage = <any>error
                            });
                            
                    }, (error) => {
                      this.errorMessage = <any>error
                    });
                        
                       
          }
        }
      ]
    });
    prompt.present();
  }
}
