import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, AlertController, ActionSheetController, Platform } from 'ionic-angular';

import { DeviceService } from './device.service';

import { Device } from './device';
import { DeviceView } from './view';
import { DeviceTag } from './device-tag';
import { DeviceUsers } from './users';

import { HomePage } from '../home/home';


import { ApiService } from '../../app/api';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
  providers: [ DeviceService ],
})
export class DevicesList {
  selectedItem: any;

  errorMessage: string;
  devices: Device[];
  mode = 'Observable';
  
  constructor(public api:ApiService, private deviceService: DeviceService, public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public platform: Platform, public actionSheetCtrl: ActionSheetController) {
    this.selectedItem = navParams.get('item');
  }

  
  escapeHtml(unsafe) {
    return unsafe.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;")
                 .replace(/"/g, "&quot;").replace(/'/g, "&#039;");
  }
  
  itemTapped(event, item) {
    this.navCtrl.push(DeviceView, {
      item: item
    });
  }
  
  logoutConfirm() {
    let confirm = this.alertCtrl.create({
        title: 'Cerrar sesión',
        message: 'Seguro que deseas salir?',
        buttons: [
            {
            text: 'Cancelar',
            handler: () => {
                console.log('Disagree clicked');
            }
            },
            {
            text: 'Cerrar sesión',
            handler: () => {
                console.log('Agree clicked');
                this.api.logged = false;
                this.api.refresh_token = "";
                this.api.token = "";
                this.navCtrl.setRoot(HomePage);
            }
            }
        ]
        });
    confirm.present();
  }
  
  
  itemPressed(event, item) {
    let actionSheet = this.actionSheetCtrl.create({
      title: item.name,
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Eliminar',
          role: 'destructive',
          icon: !this.platform.is('ios') ? 'trash' : null,
          handler: () => {
            
            let alert = this.alertCtrl.create({
              title: 'Confirmar acción',
              message: '¿Seguro que quieres eliminar el dispositivo <b>'+this.escapeHtml(item.name)+'</b>?',
              buttons: [
                {
                  text: 'Cancelar',
                  role: 'cancel',
                  handler: () => {
                  }
                },
                {
                  text: 'Eliminar',
                  handler: () => {
                            
                    this.api.getToken()
                        .then((value) => {
                            this.api.token = value.access_token;                    
        
                            this.deviceService.removeDevice(item.id)
                                              .then((value) => {
                                  this.getDevices();                
                            }, (error) => {
                                  this.errorMessage = <any>error
                            });
                    }, (error) => {
                      this.errorMessage = <any>error
                    });
                    
                  }
                }
              ]
            });
            alert.present();
          }
        },
        {
          text: 'Editar',
          icon: !this.platform.is('ios') ? 'md-create' : null,
          handler: () => {
            let prompt = this.alertCtrl.create({
              title: 'Editar dispositivo',
              message: "Introduce los datos del dispositivo",
              inputs: [
                {
                  name: 'name',
                  value: item.name,
                  placeholder: 'Nombre'
                },
                {
                  name: 'OPC_path',
                  value: item.OPC_path,
                  placeholder: 'Ruta OPC'
                },
              ],
              buttons: [
                {
                  text: 'Cancelar',
                  handler: data => {
                  }
                },
                {
                  text: 'Agregar',
                  handler: data => {
                    this.api.getToken()
                        .then((value) => {
                            this.api.token = value.access_token;                    
                              this.deviceService.editDevice(item.id, data.name, data.OPC_path)
                                              .then((value) => {
                                  this.getDevices();                
                              }, (error) => {
                                  this.errorMessage = <any>error
                              });
                    }, (error) => {
                      this.errorMessage = <any>error
                    });
                               
                  }
                }
              ]
            });
            prompt.present();
            
          }
        },
        {
          text: 'Usuarios',
          icon: !this.platform.is('ios') ? 'people' : null,
          handler: () => {
             this.navCtrl.push(DeviceUsers, {
                item: item
              });
          }
        },
        {
          text: 'Tags',
          icon: !this.platform.is('ios') ? 'md-pricetags' : null,
          handler: () => {
             this.navCtrl.push(DeviceView, {
                item: item
              });
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
          }
        }
      ]
    });

    actionSheet.present();
}
  
  
  ngOnInit() { this.getDevices(); }
  
  getDevices() {
     this.api.getToken()
        .then((value) => {
            this.api.token = value.access_token;
            if (this.api.refresh_token == "")
              this.api.refresh_token = value.refresh_token;

             this.deviceService.getDevices()
                     .then(
                       devices => this.devices = devices,
                       error =>  this.errorMessage = <any>error);
    }, (error) => {
      this.errorMessage = <any>error
    });
    
  }


  newDevice() {
    let prompt = this.alertCtrl.create({
      title: 'Nuevo dispositivo',
      message: "Introduce los datos del nuevo dispositivo",
      inputs: [
        {
          name: 'name',
          placeholder: 'Nombre'
        },
        {
          name: 'OPC_path',
          placeholder: 'Ruta OPC'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
          }
        },
        {
          text: 'Agregar',
          handler: data => {
            this.api.getToken()
                .then((value) => {
                    this.api.token = value.access_token;                    

                    this.deviceService.addDevice(data.name, data.OPC_path)
                                    .then((value) => {
                        this.getDevices();                
                    }, (error) => {
                        this.errorMessage = <any>error
                    });
            }, (error) => {
              this.errorMessage = <any>error
            });
            
          }
        }
      ]
    });
    prompt.present();
  }

}
