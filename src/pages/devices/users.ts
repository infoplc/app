import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, AlertController, ActionSheetController, Platform } from 'ionic-angular';

import { Device }              from './device';
import { User }                from './user';
import { DeviceService }       from './device.service';

import { ApiService } from '../../app/api';
@Component({
  selector: 'page-users',
  templateUrl: 'users.html',
  providers: [ DeviceService ],
})

export class DeviceUsers {
  selectedItem: any;

  errorMessage: string;
  users: User[];
  usersByRole = [];
  
  mode = 'Observable';
  
  constructor(public api:ApiService, private deviceService: DeviceService, public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public platform: Platform, public actionSheetCtrl: ActionSheetController) {
    var n = 4;
    for (var i = 0; i < n; i++)
      this.usersByRole.push([]);
    
    this.selectedItem = navParams.get('item');
  }
  
  ngOnInit() { this.getDeviceUsers(this.selectedItem.id); }
  
  test() { console.log(this.users); }
  
  getDeviceUsers(id) {
    
     this.api.getToken()
                        .then((value) => {
                            this.api.token = value.access_token;                    
                           
    this.deviceService.getDeviceUsers(id)
                     .then((value) => {
        this.users = value
        
        for (var i = 0; i < 4; i++)
          this.usersByRole[i] = [];
          
        for (let i =0; i < this.users.length; i++) {
          this.usersByRole[this.users[i].role_id].push(this.users[i]); 
        }
    }, (error) => {
       this.errorMessage = <any>error
    });
                     
                    }, (error) => {
                      this.errorMessage = <any>error
                    });
  }
  
  escapeHtml(unsafe) {
    return unsafe.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;")
                 .replace(/"/g, "&quot;").replace(/'/g, "&#039;");
  }
  
  itemOptions(event, item) {
    let actionSheet = this.actionSheetCtrl.create({
      title: item.name,
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Eliminar',
          role: 'destructive',
          icon: !this.platform.is('ios') ? 'trash' : null,
          handler: () => {
            
            let alert = this.alertCtrl.create({
              title: 'Confirmar acción',
              message: '¿Seguro que quieres eliminar el dispositivo <b>'+this.escapeHtml(item.nick)+'</b>?',
              buttons: [
                {
                  text: 'Cancelar',
                  role: 'cancel',
                  handler: () => {
                  }
                },
                {
                  text: 'Eliminar',
                  handler: () => {
                            
     this.api.getToken()
                        .then((value) => {
                            this.api.token = value.access_token;                    
                        
                       this.deviceService.removeDeviceUser(this.selectedItem.id, item.id)
                                       .then((value) => {
                           this.getDeviceUsers(this.selectedItem.id);                
                       }, (error) => {
                           this.errorMessage = <any>error
                       });
                                       
                    }, (error) => {
                      this.errorMessage = <any>error
                    });
                    
                  }
                }
              ]
            });
            alert.present();
          }
        },
        {
          text: 'Editar',
          icon: !this.platform.is('ios') ? 'md-create' : null,
          handler: () => {
            let alert = this.alertCtrl.create();
            alert.setTitle(this.escapeHtml(item.nick));
            alert.setMessage('Selecciona el nuevo rol');
            
            if (item.role_id == 1)
                alert.addInput({
                  type: 'radio',
                  label: 'Administrador',
                  value: '1',
                  checked: true
                });
            else 
                alert.addInput({
                  type: 'radio',
                  label: 'Administrador',
                  value: '1',
                });
            
            if (item.role_id == 2)
              alert.addInput({
                type: 'radio',
                label: 'Lectura/Escritura',
                value: '2',
                checked: true
              });
            else 
              alert.addInput({
                type: 'radio',
                label: 'Lectura/Escritura',
                value: '2',
              });
            
            if (item.role_id == 3)
              alert.addInput({
                type: 'radio',
                label: 'Sólo lectura',
                value: '3',
                checked: true
              });
            else 
              alert.addInput({
                type: 'radio',
                label: 'Sólo lectura',
                value: '3',
              });
        
            alert.addButton('Cancel');
            alert.addButton({
              text: 'OK',
              handler: data => {
            
            
                    this.api.getToken()
                        .then((value) => {
                            this.api.token = value.access_token;                    
                             
                          this.deviceService.editDeviceUser(this.selectedItem.id, item.id, data)
                                          .then((value) => {
                              this.getDeviceUsers(this.selectedItem.id);
                          }, (error) => {
                              this.errorMessage = <any>error
                          });
                                          
                    }, (error) => {
                      this.errorMessage = <any>error
                    });
                        
                        
              }
            });
            alert.present();
            
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
          }
        }
      ]
    });

    actionSheet.present();
}
  
  
  newUserDevice(role_id) {
    let prompt = this.alertCtrl.create({
      title: 'Nuevo usuario',
      message: "Introduce los datos del usuario",
      inputs: [
        {
          name: 'user',
          placeholder: 'Nombre de usuario o email'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            
          }
        },
        {
          text: 'Agregar',
          handler: data => {
                            
            
                    this.api.getToken()
                        .then((value) => {
                            this.api.token = value.access_token;                    
        
                            this.deviceService.addDeviceUser(this.selectedItem.id, data.user, role_id)
                                            .then((value) => {
                                this.getDeviceUsers(this.selectedItem.id);
                            }, (error) => {
                                this.errorMessage = <any>error
                            });
                                          
                    }, (error) => {
                      this.errorMessage = <any>error
                    });
                        
                       
          }
        }
      ]
    });
    
    
    prompt.present();
  }

  
}
