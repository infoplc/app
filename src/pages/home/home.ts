import { Component } from '@angular/core';
import { NavController, Platform, AlertController, ToastController } from 'ionic-angular';

import {Pfm} from "ng2-cordova-oauth/core";
import {OauthCordova} from 'ng2-cordova-oauth/platform/cordova';

import { DevicesList } from '../devices/list';

declare var window: any;

import { ApiService } from '../../app/api';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private oauth: OauthCordova = new OauthCordova();
  private pfmProvider: Pfm = new Pfm({
    clientId: "pfmApp",
    appScope: ["email"]
  })

  constructor(public api:ApiService, private navCtrl: NavController, private platform: Platform, public alertCtrl: AlertController, public toastCtrl: ToastController) { }

  goToOtherPage(OtherPage) {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.navCtrl.setRoot(DevicesList);
  }

  public apiLogin() {
    this.platform.ready().then(() => {
      this.oauth.logInVia(this.pfmProvider).then(success => {
        
        this.navCtrl.setRoot(DevicesList);
//        alert("RESULT: " + JSON.stringify(success));
        this.api.logged = true;
        this.api.auth_code = success['code'];        

        let toast = this.toastCtrl.create({
          message: 'Usuario identificado',
          duration: 3000,
          position: 'bottom'
        });
        toast.present();


      }, error => {
        //alert("ERROR: " + error);

        let alert = this.alertCtrl.create({
          title: 'Ooops!',
          subTitle: error,
          buttons: ['OK']
        });
        alert.present();

      });
    });
  }

}
