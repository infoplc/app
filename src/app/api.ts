// Promise Version
import { Injectable }              from '@angular/core';
import { Http, Response }          from '@angular/http';
import { Headers, RequestOptions, URLSearchParams } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Token } from './token';

@Injectable()
export class ApiService {
  public logged:boolean = false;
  public auth_code:string = "";
  public client:string = "pfmApp";
  public secret:string = "ever";
  
  public token:string = "";
  public refresh_token:string = "";
  
  constructor (private http: Http) {}
  
  private accessTokenUrl = 'http://api.pfm.alberto.cat/oauth2/token';
  
  getToken (): Promise<Token> {

    let data = new URLSearchParams();
    data.append('client_id', this.client);
    data.append('client_secret', this.secret);
    data.append('redirect_uri', 'http://localhost/callback');

    if (this.refresh_token == "") {
      data.append('grant_type', 'authorization_code');
      data.append('code', this.auth_code);
    }
    else
    {
      data.append('grant_type', 'refresh_token');
      data.append('refresh_token', this.refresh_token);
    }
    
      return this.http.post(this.accessTokenUrl, data)
                 .toPromise()
                 .then(this.extractData)
                 .catch(this.handleError);
  }
  
  private extractData(res: Response) {
    let body = res.json();
    return body || { };
  }
  
  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      errMsg = body.devMessage ? body.devMessage : body.userMessage;
    } else {
      errMsg = error.devMessage ? error.devMessage : error.toString();
    }
    console.error(errMsg);
    alert("[api]: " + errMsg);
    return Promise.reject(errMsg);
  }
  
}